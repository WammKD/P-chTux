<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.5" tiledversion="1.5.0" name="Serene Village" tilewidth="32" tileheight="32" tilecount="855" columns="19">
 <image source="../images/serene-village.png" width="608" height="1440"/>
 <tile id="209">
  <animation>
   <frame tileid="209" duration="100"/>
   <frame tileid="210" duration="100"/>
   <frame tileid="211" duration="100"/>
   <frame tileid="212" duration="100"/>
   <frame tileid="213" duration="100"/>
   <frame tileid="214" duration="100"/>
   <frame tileid="215" duration="100"/>
   <frame tileid="216" duration="100"/>
   <frame tileid="217" duration="100"/>
   <frame tileid="218" duration="100"/>
   <frame tileid="219" duration="100"/>
   <frame tileid="220" duration="100"/>
   <frame tileid="221" duration="100"/>
   <frame tileid="222" duration="100"/>
  </animation>
 </tile>
 <tile id="718">
  <animation>
   <frame tileid="718" duration="100"/>
   <frame tileid="719" duration="100"/>
   <frame tileid="720" duration="100"/>
   <frame tileid="721" duration="100"/>
  </animation>
 </tile>
 <wangsets>
  <wangset name="Unnamed Set" type="corner" tile="-1">
   <wangcolor name="Grass" color="#ff0000" tile="-1" probability="1"/>
   <wangcolor name="Water" color="#00ff00" tile="-1" probability="1"/>
   <wangcolor name="Dirt" color="#0000ff" tile="-1" probability="1"/>
   <wangtile tileid="3" wangid="0,1,0,1,0,1,0,1"/>
   <wangtile tileid="4" wangid="0,1,0,1,0,1,0,1"/>
   <wangtile tileid="5" wangid="0,1,0,1,0,1,0,1"/>
   <wangtile tileid="20" wangid="0,1,0,1,0,1,0,0"/>
   <wangtile tileid="21" wangid="0,0,0,1,0,1,0,1"/>
   <wangtile tileid="22" wangid="0,1,0,1,0,1,0,3"/>
   <wangtile tileid="23" wangid="0,1,0,1,0,1,0,1"/>
   <wangtile tileid="24" wangid="0,3,0,1,0,1,0,1"/>
   <wangtile tileid="25" wangid="0,3,0,1,0,3,0,3"/>
   <wangtile tileid="26" wangid="0,3,0,1,0,1,0,3"/>
   <wangtile tileid="27" wangid="0,3,0,1,0,1,0,3"/>
   <wangtile tileid="28" wangid="0,3,0,3,0,1,0,3"/>
   <wangtile tileid="39" wangid="0,1,0,1,0,0,0,1"/>
   <wangtile tileid="40" wangid="0,1,0,0,0,1,0,1"/>
   <wangtile tileid="41" wangid="0,1,0,1,0,3,0,1"/>
   <wangtile tileid="42" wangid="0,1,0,1,0,1,0,1"/>
   <wangtile tileid="43" wangid="0,1,0,3,0,1,0,1"/>
   <wangtile tileid="44" wangid="0,1,0,1,0,3,0,3"/>
   <wangtile tileid="45" wangid="0,1,0,1,0,1,0,1"/>
   <wangtile tileid="46" wangid="0,1,0,1,0,1,0,1"/>
   <wangtile tileid="47" wangid="0,3,0,3,0,1,0,1"/>
   <wangtile tileid="48" wangid="0,3,0,3,0,3,0,3"/>
   <wangtile tileid="60" wangid="0,1,0,1,0,3,0,3"/>
   <wangtile tileid="61" wangid="0,1,0,1,0,1,0,1"/>
   <wangtile tileid="62" wangid="0,3,0,3,0,1,0,1"/>
   <wangtile tileid="63" wangid="0,1,0,3,0,3,0,3"/>
   <wangtile tileid="64" wangid="0,1,0,3,0,3,0,1"/>
   <wangtile tileid="65" wangid="0,1,0,3,0,3,0,1"/>
   <wangtile tileid="66" wangid="0,3,0,3,0,3,0,1"/>
   <wangtile tileid="79" wangid="0,0,0,1,0,0,0,0"/>
   <wangtile tileid="80" wangid="0,0,0,1,0,1,0,0"/>
   <wangtile tileid="81" wangid="0,0,0,1,0,1,0,0"/>
   <wangtile tileid="82" wangid="0,0,0,1,0,1,0,0"/>
   <wangtile tileid="83" wangid="0,0,0,1,0,1,0,0"/>
   <wangtile tileid="84" wangid="0,0,0,0,0,1,0,0"/>
   <wangtile tileid="98" wangid="0,1,0,1,0,0,0,0"/>
   <wangtile tileid="99" wangid="0,1,0,1,0,1,0,1"/>
   <wangtile tileid="100" wangid="0,1,0,1,0,1,0,1"/>
   <wangtile tileid="101" wangid="0,1,0,1,0,1,0,1"/>
   <wangtile tileid="102" wangid="0,1,0,1,0,1,0,1"/>
   <wangtile tileid="103" wangid="0,0,0,0,0,1,0,1"/>
   <wangtile tileid="117" wangid="0,1,0,0,0,0,0,0"/>
   <wangtile tileid="118" wangid="0,1,0,0,0,0,0,1"/>
   <wangtile tileid="119" wangid="0,1,0,0,0,0,0,1"/>
   <wangtile tileid="120" wangid="0,1,0,0,0,0,0,1"/>
   <wangtile tileid="121" wangid="0,1,0,0,0,0,0,1"/>
   <wangtile tileid="122" wangid="0,0,0,0,0,0,0,1"/>
  </wangset>
 </wangsets>
</tileset>
